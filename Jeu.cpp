#include <iostream>
#include "Jeu.h"

using namespace std;

void initJeu(Jeu *jeu, int modeJeu)
{
    jeu->modeJeu = modeJeu;
    jeu->fin = false;
    string nomJoueur;
    // Initialisation du tableau de jetons à null
    for (int i=0; i<8; i++)
    {
        for (int j=0; j<8; j++)
        {
            Jeton *nouveauJeton = new Jeton;
            nouveauJeton->couleur = ' ';
            nouveauJeton->pos_X = i+1;
            nouveauJeton->pos_Y = j+1;
            jeu->jetons[i][j] = nouveauJeton;
        }
    }

    //Joueur 1
    cout << "Nom du joueur 1 : ";
    cin >> nomJoueur;
    initJoueur(&jeu->joueurs[0], nomJoueur, 'X', false);

    nomJoueur = "";
    //Joueur 2
    if (modeJeu == 1)
    {
        cout << "Nom du joueur 2 : ";
        cin >> nomJoueur;
        initJoueur(&jeu->joueurs[1], nomJoueur, 'O', false);
    }
    else
    {
        initJoueur(&jeu->joueurs[1], "Monsieur AI", 'O', true);
    }
    
    // Recap
    cout << jeu->joueurs[0].nom_joueur << " prend les pions noirs (" << jeu->joueurs[0].couleur << ")" << endl;
    cout << jeu->joueurs[1].nom_joueur << " prend les pions blancs (" << jeu->joueurs[1].couleur << ")" << endl;
    cout << "La partie peut commencer !"<< endl;

    //On place les 4 jetons de début de jeu
    placerJeton(jeu->jetons[3][3],'O');
    placerJeton(jeu->jetons[4][4],'O');
    placerJeton(jeu->jetons[3][4],'X');
    placerJeton(jeu->jetons[4][3],'X');
}

void afficheGrille(Jeu jeu)
{
    //Parcours du tableau de jetons pour placer visuellement sur la grille les jetons
    cout << "    A   B   C   D   E   F   G   H  " << endl;
    cout << "  +---+---+---+---+---+---+---+---+" << endl;

    for (int i=0; i<8; i++)
    {
        cout << i+1 << " ";

        for (int j=0; j<8; j++)
        {
            cout << "| " << jeu.jetons[i][j]->couleur << " ";
        }
        cout << "|" << endl;
        cout << "  +---+---+---+---+---+---+---+---+" << endl;
    }
}

void tourJoueur(Jeu *jeu, int numeroTour) 
{ 
    int X;
    char lettre_Y;
    int Y;

    nbJeton(jeu);
    afficheNbJeton(jeu->joueurs[0]);
    afficheNbJeton(jeu->joueurs[1]);


    cout << "C'est au tour de " << jeu->joueurs[numeroTour %2].nom_joueur << endl;
    if (grilleRemplie(*jeu))
    {
        cout << "La grille est remplie. La partie est terminée. " << endl;
        jeu->fin = true;
    }
    else if (nbPossibilite(*jeu, numeroTour %2) == 0 && nbPossibilite(*jeu, (numeroTour+1) %2) == 0)
    {
        cout << "Aucun des joueurs ne peut jouer. La partie est terminée. " << endl;
        jeu->fin = true;
    }
    else if (nbPossibilite(*jeu, numeroTour %2) == 0)
    {
        cout << jeu->joueurs[numeroTour %2].nom_joueur << " n'a pas la possibilité de jouer." << endl;
        numeroTour++ ;
    }
    else if(!jeu->joueurs[numeroTour %2].AI)
    {
        affichePossibilite(*jeu, numeroTour %2);
        do
        {
            cout << "Entrez l'emplacement horizontal [CHIFFRE]: ";
            cin >> X;
        }
        while (X < 0 || X > 9);
        
        do
        {
            cout << "Entrez l'emplacement vertical [LETTRE]: ";
            cin >> lettre_Y;
        }
        while ((int)lettre_Y < 64 || (int)lettre_Y > 73);

        Y = (int)lettre_Y - 64;

        if (voisinAdverse(*jeu, jeu->joueurs[numeroTour %2].couleur, X, Y) && jeu->jetons[X-1][Y-1]->couleur == ' ' && capture(jeu,jeu->joueurs[numeroTour %2].couleur, X, Y, true) > 0)
        {
            placerJeton(jeu->jetons[X-1][Y-1], jeu->joueurs[numeroTour %2].couleur);
        }
        else
        {
            cout << "Emplacement incorrect" << endl;
            tourJoueur(jeu, numeroTour);
        }
    }
    else if(jeu->joueurs[numeroTour%2].AI)
    {
        TourAI(jeu);
    }
}

void nbJeton(Jeu *jeu)
{
    jeu->joueurs[0].nb_jetons = 0;
    jeu->joueurs[1].nb_jetons = 0;

    for (int i=0; i<=7; i++)
    {
        for (int j=0; j<=7; j++)
        {
            if (jeu->jetons[i][j]->couleur == 'X')
                jeu->joueurs[0].nb_jetons++;
            else if (jeu->jetons[i][j]->couleur == 'O')
                jeu->joueurs[1].nb_jetons++;
        }
    }
}

//fonction qui vérifie si la case choisie est voisine d'un pion adverse
bool voisinAdverse(Jeu jeu, char couleur, int X, int Y) {

    //cherche couleur adverse
    char couleurAdverse;
    if (couleur == 'X')
        couleurAdverse = 'O';
    else
        couleurAdverse = 'X';

    //parcours des voisins
    for (int i = X-2; i <= X; i++)
    {
        for (int j = Y-2; j <= Y; j++)
        {
            if (i>=0 && i<8 && j>=0 && j<8)
            {
                if (jeu.jetons[i][j]->couleur == couleurAdverse)
                    return true;
            }
        }
    }
    return false;
}

int nbPossibilite(Jeu jeu, int numeroJoueur)
{
    int nbPossibilite = 0;
    for (int i=0; i<=7; i++)
    {
        for (int j=0; j<=7; j++)
        {
            if (voisinAdverse(jeu, jeu.joueurs[numeroJoueur].couleur, jeu.jetons[i][j]->pos_X, jeu.jetons[i][j]->pos_Y) && jeu.jetons[i][j]->couleur == ' ' && capture(&jeu, jeu.joueurs[numeroJoueur].couleur, jeu.jetons[i][j]->pos_X, jeu.jetons[i][j]->pos_Y, false)>=1)
            {
                nbPossibilite++ ;
            }
        }
    }
    return nbPossibilite;
}

void TourAI(Jeu *jeu)
{
    int nbCaptureMax = 0;
    Jeton *jetonCapture;
    for (int i=0; i<=7; i++)
    {
        for (int j=0; j<=7; j++)
        {
            if (voisinAdverse(*jeu, 'O', jeu->jetons[i][j]->pos_X, jeu->jetons[i][j]->pos_Y) && jeu->jetons[i][j]->couleur == ' ' && capture(jeu, 'O', jeu->jetons[i][j]->pos_X, jeu->jetons[i][j]->pos_Y, false)>=1)
            {
                if (capture(jeu, 'O', jeu->jetons[i][j]->pos_X, jeu->jetons[i][j]->pos_Y, false) > nbCaptureMax)
                {
                    nbCaptureMax = capture(jeu, 'O', jeu->jetons[i][j]->pos_X, jeu->jetons[i][j]->pos_Y, false);
                    jetonCapture = jeu->jetons[i][j];
                }
            }
        }
    }

    jetonCapture->couleur = 'O';
    capture(jeu, 'O', jetonCapture->pos_X, jetonCapture->pos_Y, true);
}

void affichePossibilite(Jeu jeu, int numeroJoueur)
{
    cout << "Vous pouvez placer votre jeton sur ces cases : ";
    for (int i=0; i<=7; i++)
    {
        for (int j=0; j<=7; j++)
        {
            if (voisinAdverse(jeu, jeu.joueurs[numeroJoueur].couleur, jeu.jetons[i][j]->pos_X, jeu.jetons[i][j]->pos_Y) && jeu.jetons[i][j]->couleur == ' ' && capture(&jeu, jeu.joueurs[numeroJoueur].couleur, jeu.jetons[i][j]->pos_X, jeu.jetons[i][j]->pos_Y, false)>=1)
            {
                cout << jeu.jetons[i][j]->pos_X << (char)(jeu.jetons[i][j]->pos_Y + 64) << ", ";
                //cout << " (" << capture(&jeu, jeu.joueurs[numeroJoueur].couleur, jeu.jetons[i][j]->pos_X, jeu.jetons[i][j]->pos_Y, false) << " capture)" << ", ";
            }
        }
    }
    cout << endl;
}

int capture(Jeu *jeu, char couleur, int X, int Y, bool capturePossible)
{
    Jeton *jetonsCapture[8] = {}; //tableau éphémère de pointeur sur jeton
    int taille = 0; //nombres de jetons ajoutés au tableau
    int nbCapture = 0; //nombre de capture (pour la condition d'au moins une capture)

    //cherche couleur adverse
    char couleurAdverse;
    if (couleur == 'X')
        couleurAdverse = 'O';
    else
        couleurAdverse = 'X';

    //Nord
    for (int i=X-2; i>=0; i--) //parcours jetons au dessus
    {
        if (jeu->jetons[i][Y-1]->couleur == ' ') //s'il n'y a pas de pion dans la ligne on sort de la boucle
            break;
        else if (jeu->jetons[i][Y-1]->couleur == couleurAdverse) //si c'est un pion adverse, on l'ajoute à notre tableau éphémère
        {
            if (capturePossible)
                jetonsCapture[taille]=jeu->jetons[i][Y-1];

            taille++;
        }
        else if (jeu->jetons[i][Y-1]->couleur == couleur) //s'il y a bien un pion à nous on peut capturer et on sort de la boucle
        {
            for (int i=0; i<taille; i++)
            {
                nbCapture++;
                if (capturePossible)
                {
                    jetonsCapture[i]->couleur = couleur; //changement de couleur
                    jetonsCapture[i] = NULL;
                }
            }
            break;
        }    
    }
    taille = 0;

  
    //NordEst
    int i = X-2;
    int j = Y;
    while (i>=0 && j<=7) //parcours jetons au-dessus à droite
    {
        if (jeu->jetons[i][j]->couleur == ' ') //s'il n'y a pas de pion dans la ligne on sort de la boucle
            break;
        else if (jeu->jetons[i][j]->couleur == couleurAdverse) //si c'est un pion adverse, on l'ajoute à notre tableau éphémère
        {
            if (capturePossible)
                jetonsCapture[taille]=jeu->jetons[i][j];

            taille++;
        }
        else if (jeu->jetons[i][j]->couleur == couleur) //s'il y a bien un pion à nous on peut capturer et on sort de la boucle
        {
            for (int i=0; i<taille; i++)
            {
                nbCapture++;
                if (capturePossible)
                {
                    jetonsCapture[i]->couleur = couleur; //changement de couleur
                    jetonsCapture[i] = NULL;
                }
            }
            break;
        }    
        i--;
        j++;
    }
    taille = 0;


    //Est
    for (int j=Y; j<=7; j++) //parcours jetons à droite
    {
        if (jeu->jetons[X-1][j]->couleur == ' ') //s'il n'y a pas de pion dans la ligne on sort de la boucle
            break;
        else if (jeu->jetons[X-1][j]->couleur == couleurAdverse) //si c'est un pion adverse, on l'ajoute à notre tableau éphémère
        {
            if (capturePossible)
                jetonsCapture[taille]=jeu->jetons[X-1][j];

            taille++;
        }
        else if (jeu->jetons[X-1][j]->couleur == couleur) //s'il y a bien un pion à nous on peut capturer et on sort de la boucle
        {
            for (int i=0; i<taille; i++)
            {
                nbCapture++;
                if (capturePossible)
                {
                    jetonsCapture[i]->couleur = couleur; //changement de couleur
                    jetonsCapture[i] = NULL;
                }
            }
            break;
        }    
    }
    taille = 0;


    //SudEst
    i = X;
    j = Y;
    while (i<=7 && j<=7) //parcours jetons au-dessous à droite
    {
        if (jeu->jetons[i][j]->couleur == ' ') //s'il n'y a pas de pion dans la ligne on sort de la boucle
            break;
        else if (jeu->jetons[i][j]->couleur == couleurAdverse) //si c'est un pion adverse, on l'ajoute à notre tableau éphémère
        {
            if (capturePossible)
                jetonsCapture[taille]=jeu->jetons[i][j];

            taille++;
        }
        else if (jeu->jetons[i][j]->couleur == couleur) //s'il y a bien un pion à nous on peut capturer et on sort de la boucle
        {
            for (int i=0; i<taille; i++)
            {
                nbCapture++;
                if (capturePossible)
                {
                    jetonsCapture[i]->couleur = couleur; //changement de couleur
                    jetonsCapture[i] = NULL;
                }
            }
            break;
        }    
        i++;
        j++;
    }
    taille = 0;


    //Sud
    for (int i=X; i<=7; i++) //parcours jetons au dessous
    {
        if (jeu->jetons[i][Y-1]->couleur == ' ') //s'il n'y a pas de pion dans la ligne on sort de la boucle
            break;
        else if (jeu->jetons[i][Y-1]->couleur == couleurAdverse) //si c'est un pion adverse, on l'ajoute à notre tableau éphémère
        {
            if (capturePossible)
                jetonsCapture[taille]=jeu->jetons[i][Y-1];

            taille++;
        }
        else if (jeu->jetons[i][Y-1]->couleur == couleur) //s'il y a bien un pion à nous on peut capturer et on sort de la boucle
        {
            for (int i=0; i<taille; i++)
            {
                nbCapture++;
                if (capturePossible)
                {
                    jetonsCapture[i]->couleur = couleur; //changement de couleur
                    jetonsCapture[i] = NULL;
                }
            }
            break;
        }    
    }
    taille = 0;


    //SudOuest
    i = X;
    j = Y-2;
    while (i<=7 && j>=0) //parcours jetons au-dessous à gauche
    {
        if (jeu->jetons[i][j]->couleur == ' ') //s'il n'y a pas de pion dans la ligne on sort de la boucle
            break;
        else if (jeu->jetons[i][j]->couleur == couleurAdverse) //si c'est un pion adverse, on l'ajoute à notre tableau éphémère
        {
            if (capturePossible)
                jetonsCapture[taille]=jeu->jetons[i][j];

            taille++;
        }
        else if (jeu->jetons[i][j]->couleur == couleur) //s'il y a bien un pion à nous on peut capturer et on sort de la boucle
        {
            for (int i=0; i<taille; i++)
            {
                nbCapture++;
                if (capturePossible)
                {
                    jetonsCapture[i]->couleur = couleur; //changement de couleur
                    jetonsCapture[i] = NULL;
                }
            }
            break;
        }    
        i++;
        j--;
    }
    taille = 0;


    //Ouest
    for (int j=Y-2; j>=0; j--) //parcours jetons à gauche
    {
        if (jeu->jetons[X-1][j]->couleur == ' ') //s'il n'y a pas de pion dans la ligne on sort de la boucle
            break;
        else if (jeu->jetons[X-1][j]->couleur == couleurAdverse) //si c'est un pion adverse, on l'ajoute à notre tableau éphémère
        {
            if (capturePossible)
                jetonsCapture[taille]=jeu->jetons[X-1][j];

            taille++;
        }
        else if (jeu->jetons[X-1][j]->couleur == couleur) //s'il y a bien un pion à nous on peut capturer et on sort de la boucle
        {
            for (int i=0; i<taille; i++)
            {
                nbCapture++;
                if (capturePossible)
                {
                    jetonsCapture[i]->couleur = couleur; //changement de couleur
                    jetonsCapture[i] = NULL;
                }
            }
            break;
        }    
    }
    taille = 0;

   
    //NordOuest
    i = X-2;
    j = Y-2;
    while (i>=0 && j>=0) //parcours jetons au-dessus à gauche
    {
        if (jeu->jetons[i][j]->couleur == ' ') //s'il n'y a pas de pion dans la ligne on sort de la boucle
            break;
        else if (jeu->jetons[i][j]->couleur == couleurAdverse) //si c'est un pion adverse, on l'ajoute à notre tableau éphémère
        {
            if (capturePossible)
                jetonsCapture[taille]=jeu->jetons[i][j];

            taille++;
        }
        else if (jeu->jetons[i][j]->couleur == couleur) //s'il y a bien un pion à nous on peut capturer et on sort de la boucle
        {
            for (int i=0; i<taille; i++)
            {
                nbCapture++;
                if (capturePossible)
                {
                    jetonsCapture[i]->couleur = couleur; //changement de couleur
                    jetonsCapture[i] = NULL;
                }
            }
            break;
        }    
        i--;
        j--;
    }
    taille = 0;
    
    return nbCapture;
}

//fonction qui renvoie true si toutes les cases sont remplies
bool grilleRemplie(Jeu jeu)
{
    for (int i=0; i<8; i++)
    {
        for (int j=0; j<8; j++)
        {
            if (jeu.jetons[i][j]->couleur == ' ')
                return false;
        }
    }
    return true;
}

//fonction qui annonce les scores lorsque la partie est finie
void resultat(Jeu jeu)
{
    if (jeu.joueurs[0].nb_jetons > jeu.joueurs[1].nb_jetons)
    {
        cout << jeu.joueurs[0].nom_joueur << " A GAGNÉ ! FÉLICITATION !" << endl;
        cout << jeu.joueurs[1].nom_joueur << " est un LOOSER" << endl;
    }
    else if (jeu.joueurs[1].nb_jetons > jeu.joueurs[0].nb_jetons)
    {
        cout << jeu.joueurs[1].nom_joueur << " A GAGNÉ ! FÉLICITATION !" << endl;
        cout << jeu.joueurs[0].nom_joueur << " est un LOOSER" << endl;
    }
    else if (jeu.joueurs[0].nb_jetons == jeu.joueurs[1].nb_jetons)
    {
        cout << "ÉGALITÉ ! Refaites une partie pour vous départager" << endl;
    }
}