#ifndef JETON_H
#define JETON_H

struct Jeton{
    char couleur;
    int pos_X;
    int pos_Y;
};

void placerJeton(Jeton *jeton, char couleur);

#endif