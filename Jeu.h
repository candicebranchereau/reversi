#ifndef JEU_H
#define JEU_H

#include "Jeton.h"
#include "Joueur.h"

struct Jeu {
    Joueur joueurs[2];
    Jeton *jetons[8][8];
    int modeJeu;
    bool fin;
};

void initJeu(Jeu *jeu, int modeJeu);
void afficheGrille(Jeu jeu);
void tourJoueur(Jeu *jeu, int numeroTour);
void TourAI(Jeu *jeu);
void nbJeton(Jeu *jeu);
bool voisinAdverse(Jeu jeu, char couleur, int X, int Y);
int capture(Jeu *jeu, char couleur, int X, int Y, bool capturePossible);
void affichePossibilite(Jeu jeu, int numeroJoueur);
int nbPossibilite(Jeu jeu, int numeroJoueur);
void resultat(Jeu jeu);
bool grilleRemplie(Jeu jeu);

#endif