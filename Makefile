# makefile2 correspondant a l'exercice 2, TP8

# Dans un fichier makefile, on peut utiliser de variables.
# monvar = valeur    pour affecter
# $(monvar)          pour recuperer la valeur

# De plus, il y a quelques variables internes, qu'on peut utiliser:
# $@ pour nom de cible
# $< pour nom de la premiere dependance
# $^ pour toute la liste des dépendances

# le suivant alors fait precisement la meme chose que makefile1:


CC=g++
CFLAGS=-Wall -ansi


moncode: main.o Jeu.o Jeton.o Joueur.o
	$(CC) -o $@ $^
	
main.o: main.cpp Jeu.h Jeton.h
	$(CC) -o $@ -c $< $(CFLAGS)

Jeu.o: Jeu.cpp Jeu.h
	$(CC) -o $@ -c $< $(CFLAGS)

Jeton.o: Jeton.cpp Jeton.h
	$(CC) -o $@ -c $< $(CFLAGS)

Joueur.o: Joueur.cpp Joueur.h
	$(CC) -o $@ -c $< $(CFLAGS)

clean:
	rm -rf *.o

# mais en plus, on va effacer les fichiers .o

# On peut en faire encore plus ; dans certains cas, par exemple, certaines
# regles peuvent etre omises, car définies implicitement.
#
# Si vous voulez en connaitre plus, pour l'instant, cherchez en ligne.
