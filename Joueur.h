#ifndef JOUEUR_H
#define JOUEUR_H

using namespace std;

struct Joueur{
    char couleur;
    string nom_joueur;
    int nb_jetons;
    bool AI;
};

void initJoueur(Joueur *joueur, string nomJoueur, char couleur, bool AI);
void afficheNbJeton(Joueur joueur);


#endif