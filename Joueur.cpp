#include <iostream>
#include <string.h>
#include "Joueur.h"

using namespace std;


void initJoueur(Joueur *joueur, string nomJoueur, char couleur, bool AI) {
    
    joueur->couleur = couleur;
    joueur->nom_joueur = nomJoueur;
    joueur->nb_jetons = 0;
    joueur->AI = AI;
}

void afficheNbJeton(Joueur joueur)
{
    cout << joueur.nom_joueur << " a " << joueur.nb_jetons ;
    if (joueur.nb_jetons <= 1)
        cout << " jeton " << endl;
    else
        cout << " jetons " << endl;
}
