#include <iostream>
#include "Jeu.h"

using namespace std;

int main() {

    Jeu jeu;
    int numeroTour = 0;
    int choix;
   
    cout << "Contre qui voulez-vous jouer ?" << endl;
    cout << "JOUEUR RÉEL [1] ou JOUEUR ARTIFICIEL [2] ? : " ;
    do
    {
        cin >> choix;
    }
    while(choix!=1 && choix!=2);
    
    initJeu(&jeu, choix);

    while (!jeu.fin)
    {
        afficheGrille(jeu);
        tourJoueur(&jeu, numeroTour);
        numeroTour++;
    }
    afficheGrille(jeu);
    resultat(jeu);
    
    return 0;
}
